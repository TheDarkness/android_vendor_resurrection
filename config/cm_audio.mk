#
# CyanogenMod Audio Files
#

ALARM_PATH := vendor/cm/prebuilt/common/media/audio/alarms
NOTIFICATION_PATH := vendor/cm/prebuilt/common/media/audio/notifications
RINGTONE_PATH := vendor/cm/prebuilt/common/media/audio/ringtones
UI_PATH := vendor/cm/prebuilt/common/media/audio/ui

# Notifications
PRODUCT_COPY_FILES += \
    $(NOTIFICATION_PATH)/CyanMessage.ogg:system/media/audio/notifications/CyanMessage.ogg \
    $(NOTIFICATION_PATH)/Content.mp3:system/media/audio/notifications/Content.mp3 \
    $(NOTIFICATION_PATH)/Lime.mp3:system/media/audio/notifications/Lime.mp3 \
    $(NOTIFICATION_PATH)/Postman.ogg:system/media/audio/notifications/Postman.ogg \
    $(NOTIFICATION_PATH)/PureBell.ogg:system/media/audio/notifications/PureBell.ogg \
    $(NOTIFICATION_PATH)/Venture.mp3:system/media/audio/notifications/Venture.mp3

# Ringtones
ifeq ($(TARGET_NEEDS_BOOSTED_SOUNDS),true)
PRODUCT_COPY_FILES += \
	$(RINGTONE_PATH)/boosted/Resurrection.mp3:system/media/audio/ringtones/Resurrection.mp3

else
PRODUCT_COPY_FILES += \
    $(RINGTONE_PATH)/Resurrection.mp3:system/media/audio/ringtones/Resurrection.mp3 \
    $(RINGTONE_PATH)/FurElise.ogg:system/media/audio/ringtones/FurElise.ogg
endif
